var share_module = angular.module('share_module', ['ui.router','ngAnimate', 'ngSanitize','ui.bootstrap','nvd3']);

share_module.config(['$stateProvider','$urlRouterProvider','$locationProvider','$provide', function($stateProvider, $urlRouterProvider,$locationProvider,$provide){
	$urlRouterProvider.otherwise('home');   
	$stateProvider.state('home',{
	        url: '/home',
	        templateUrl: 'app/components/home/partials/home.html',
	        controller : "homeController",
	      })
	      .state('about',{
	        url: '/about',
	        templateUrl: 'about.html'
	      })
	      .state('contact',{
	        url: '/contact',
	        templateUrl: 'contact.html'
	      }); 
}]);