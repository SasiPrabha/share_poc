	angular.module('share_module').directive('navbar', function() {
  return {
	restrict: 'E',
	templateUrl: 'app/shared/navbar/partials/navbarDirective.html',
	controller : 'navCtrl'
  };
});
angular.module('share_module').controller('navCtrl',['$scope','$uibModal','$rootScope','$window','$timeout','$document','$location', function($scope,$uibModal,$rootScope,$window,$timeout,$document,$location) {
	$rootScope.accessTokenAvailable = false;
	$scope.showContentHelp=false;
	$scope.arrowUpHelp=false;
	$scope.arrowDownHelp=true;
	$scope.showContentUser=false;
	$scope.arrowUpUser=false;
	$scope.arrowDownUser=true;
	
	$scope.emplView=false;
	
	
	/*
	
	$scope.hideHelp=function(){
		$scope.showContentHelp=false;
	};
	$scope.hideUser=function(){
		$scope.showContentUser=false;
	};
	$scope.showDropDownHelp=function(){
		$scope.showContentHelp=true;
		$scope.arrowUpHelp=true;
		$scope.arrowDownHelp=false;
	};
	$scope.hideDropDownHelp=function(){
		$scope.showContentHelp=false;
		$scope.arrowDownHelp=true;
		$scope.arrowUpHelp=false;
	};
	$scope.showDropDownUser=function(){
		$scope.showContentUser=true;
		$scope.arrowUpUser=true;
		$scope.arrowDownUser=false;
	};
	$scope.hideDropDownUser=function(){
		$scope.showContentUser=false;
		$scope.arrowDownUser=true;
		$scope.arrowUpUser=false;
	};
	
	
	*/
	
	
	
	
	/*
	$scope.open = function(size, template) {
	      var modalInstance = $uibModal.open({
	          templateUrl: template || 'myModalContent.html',
	          controller: 'ModalInstanceCtrl',
	          size: size
	        });
	    };
	   */ 
	  }]);

/*
angular.module('visibility_module').controller('ModalInstanceCtrl',['$scope','$uibModalInstance','vizAPIService','$rootScope','$timeout','managerService', function($scope,$uibModalInstance,vizAPIService,$rootScope,$timeout,managerService) {
		$scope.searchUserDatas=[];
		$scope.getSearchData = function(n){
			$timeout(function(){
				if($scope.userName === n){
					vizAPIService.apiGetRequest(null, "users?searchText="+n+"&userName=", null).then(
						function(response) {
							$scope.searchUserDatas=[];
							$scope.nav = response.data.data;
							for(var i=0;i<$scope.nav["P_USERLIST"].length;i++){
								$scope.searchUserDatas.push($scope.nav["P_USERLIST"][i]);
							}
						}
					)
				}
			},1000);
		};
		$scope.changeName=function(){
			if($scope.userName.length>3){
				$scope.getSearchData($scope.userName);
			}
		};
		$scope.changeUser=function(index){
			managerService.setTeamData(null);
			managerService.setEmpId(null);
			$rootScope.selectedID.id=index;
			$rootScope.getIdData();
			$scope.cancel();
		};
	  $scope.cancel = function() {
		  $uibModalInstance.dismiss('cancel');
	  };
	}]);
*/